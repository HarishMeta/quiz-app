Quiz Application MicroService

  Database :
  
  Postgres
  
  Backend : (Spring Boot) 
  
  This is an open ended use case where we can enhance a lot of features, but with the time i have , below are the features implemented.
  
  When you login to the application, you will be provided an link to start quiz. It consits of 10 questions. 
  You keep answering the questions , the responses will be recorded in the database and after the final question, the overall result will be displayed.
  
  
  Things missed because of the time crunch :
  
  1. Logging could have been done much better.
  2. Exception handling could be added using @Controlleradvice for client side and server side validations.
  



To run auth microservice locally using docker, follow the below steps :

    1.  sudo docker pull registry.gitlab.com/harishmeta/quiz-app/quizappv1
    2.  sudo docker run --name authsvc_container --rm -d -p 8080:8080 registry.gitlab.com/harishmeta/quiz-app/quizappv1:latest