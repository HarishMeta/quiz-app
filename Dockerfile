FROM java:8-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} quizsvc.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/quizsvc.jar"]
