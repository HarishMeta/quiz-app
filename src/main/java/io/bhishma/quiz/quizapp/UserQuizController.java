package io.bhishma.quiz.quizapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bhishma.platform.boot.user.User;
import io.bhishma.platform.boot.user.UserContext;
import io.bhishma.quiz.quizapp.entity.UserEntity;
import io.bhishma.quiz.quizapp.model.UserQuiz;
import io.bhishma.quiz.quizapp.service.QuizService;
import io.bhishma.quiz.quizapp.service.UserQuizService;

@RestController
@RequestMapping("/api/v1/user/quiz")
public class UserQuizController {
	
	@Autowired
	private UserQuizService userQuizService;
	
	@Autowired
	private QuizService quizService;
	
	@PostMapping
	public ResponseEntity<UserQuiz> update(@RequestBody final UserQuiz userQuiz) {	
		User user = UserContext.getUser();
		
		if(userQuiz.getUserId() == null) {
			UserEntity ue = quizService.findUserByEmail(user.getEmail());
			userQuiz.setUserId(ue.getId());
		}
		userQuizService.update(userQuiz);
		return new ResponseEntity<UserQuiz>(userQuiz, HttpStatus.OK);		
	}

}
