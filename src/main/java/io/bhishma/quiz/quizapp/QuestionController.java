package io.bhishma.quiz.quizapp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bhishma.quiz.quizapp.entity.QuestionEntity;
import io.bhishma.quiz.quizapp.model.Question;
import io.bhishma.quiz.quizapp.service.QuestionService;

@RestController
@RequestMapping("/api/v1/quiz/{quiz-id}/questions")
public class QuestionController {
	
	@Autowired
	private QuestionService questionService;

	@GetMapping
	public ResponseEntity<Object> fetchQuestions(@PathVariable("quiz-id") Integer quizId) {
		final List<QuestionEntity> questions = questionService.findByQuizId(quizId);
		return new ResponseEntity<Object>(questions, HttpStatus.OK);
	}
	
	@GetMapping("/{order}")
	public ResponseEntity<Object> fetchQuestion(@PathVariable("quiz-id") Integer quizId,
												@PathVariable("order") Integer order) {
		final Question question = questionService.findByQuizIdAndOrder(quizId, order);
		return new ResponseEntity<Object>(question, HttpStatus.OK);
	}
	
}
