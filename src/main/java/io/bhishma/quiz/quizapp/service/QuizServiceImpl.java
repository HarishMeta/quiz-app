package io.bhishma.quiz.quizapp.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.bhishma.quiz.quizapp.entity.QuizEntity;
import io.bhishma.quiz.quizapp.entity.UserEntity;
import io.bhishma.quiz.quizapp.model.Quiz;
import io.bhishma.quiz.quizapp.repository.QuizRepository;
import io.bhishma.quiz.quizapp.repository.UserRepository;

@Component
public class QuizServiceImpl implements QuizService {
	
	private Logger LOG = LoggerFactory.getLogger(QuizServiceImpl.class);
	
	@Autowired
	private QuizRepository quizRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void create(final Quiz quiz) {
		
		LOG.info("Creating Quiz with name {}", quiz.getName());
		
		final QuizEntity quizEntity = new QuizEntity();
		quizEntity.setName(quiz.getName());
		quizEntity.setDescritpion(quiz.getDescription());
		quizEntity.setCreatedBy("BATMAN");
		quizEntity.setUpdatedBy("BATMAN");
		
		final Date date = new Date();
		quizEntity.setCreatedDate(date);
		quizEntity.setUpdatedDate(date);

		quizRepository.save(quizEntity);
		
		LOG.info("Successfully Created Quiz with name {}", quiz.getName());
		
	}

	@Override
	public UserEntity findUserByEmail(String email) {
		
		List<UserEntity> users = userRepository.findByEmail(email);
		
		if(!CollectionUtils.isEmpty(users)) {
			return users.get(0);
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
