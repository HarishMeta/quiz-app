package io.bhishma.quiz.quizapp.service;

import io.bhishma.quiz.quizapp.model.UserAnswer;

public interface AnswerService {
	
	void save(UserAnswer userAnswer);

}
