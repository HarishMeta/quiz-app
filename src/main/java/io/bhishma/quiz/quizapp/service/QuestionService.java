package io.bhishma.quiz.quizapp.service;

import java.util.List;

import io.bhishma.quiz.quizapp.entity.QuestionEntity;
import io.bhishma.quiz.quizapp.model.Question;

public interface QuestionService {
	
	public List<QuestionEntity> findByQuizId(Integer quizId);
	
	public Question findByQuizIdAndOrder(Integer quizId, Integer order);


}
