package io.bhishma.quiz.quizapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import io.bhishma.quiz.quizapp.entity.QuizEntity;
import io.bhishma.quiz.quizapp.entity.UserQuizEntity;
import io.bhishma.quiz.quizapp.entity.UserQuizStatusEntity;
import io.bhishma.quiz.quizapp.model.QuizResult;
import io.bhishma.quiz.quizapp.model.UserQuiz;
import io.bhishma.quiz.quizapp.repository.QuizRepository;
import io.bhishma.quiz.quizapp.repository.UserQuizRepository;
import io.bhishma.quiz.quizapp.repository.UserQuizStatusRepository;

@Component
public class UserQuizServiceImpl implements UserQuizService {
	
	@Autowired
	private UserQuizRepository userQuizRepository;
	
	@Autowired
	private UserQuizStatusRepository userQuizStatusRepository;
	
	@Autowired
	private QuizRepository quizRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private String COUNT_SQL = "select COUNT(*) from quizapp.user_quiz_status uqs, quizapp.choices c "
			+ "where uqs.choice_id = c.id and uqs.user_id = ? and uqs.quiz_id = ? and c.is_right = ? ";
	

	@Override
	@Transactional
	public void update(final UserQuiz userQuiz) {
		
		final List<UserQuizEntity> userQuizList = userQuizRepository.findByUserIdAndQuizId(userQuiz.getUserId(), userQuiz.getQuizId());
		
		if(!CollectionUtils.isEmpty(userQuizList)) {			
			userQuizRepository.updateUser(userQuiz.getStatus(), userQuiz.getUserId() , userQuiz.getQuizId());					
		}else {
			final UserQuizEntity ue = new UserQuizEntity();
			ue.setQuizId(userQuiz.getQuizId());
			ue.setUserId(userQuiz.getUserId());
			ue.setStatus(userQuiz.getStatus());
			userQuizRepository.save(ue);
		}
	}

	@Override
	public QuizResult getResult(int userId, int quizId) {
		final List<UserQuizStatusEntity> userQuizStatusList = userQuizStatusRepository.findByUserIdAndQuizId(userId, quizId);
		final Integer correctCount = jdbcTemplate.queryForObject(COUNT_SQL, new Object[] {userId, quizId, true}, Integer.class);

		Integer attemptedCount = 0;
		
		if(userQuizStatusList!= null) {
			attemptedCount = userQuizStatusList.size();
		}
		
		QuizResult qr = new QuizResult();
		qr.setCorrect(correctCount);
		qr.setTotalQuestions(10);
		qr.setAttempted(attemptedCount);
		qr.setWrong(10-correctCount);
		
		return qr;
	}

	@Override
	public QuizEntity loadQuiz(int userId) {
		
		List<UserQuizEntity> userQuizList = userQuizRepository.findByUserId(userId);
		
		if(!CollectionUtils.isEmpty(userQuizList)) {
			return quizRepository.findById(Long.valueOf(userQuizList.get(0).getQuizId())).get();
		}
		
		// TODO Auto-generated method stub
		return null;
	}

}
