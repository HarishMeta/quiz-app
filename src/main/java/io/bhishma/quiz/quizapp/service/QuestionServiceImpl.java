package io.bhishma.quiz.quizapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import io.bhishma.quiz.quizapp.entity.ChoiceEntity;
import io.bhishma.quiz.quizapp.entity.QuestionEntity;
import io.bhishma.quiz.quizapp.entity.UserQuizStatusEntity;
import io.bhishma.quiz.quizapp.model.Choice;
import io.bhishma.quiz.quizapp.model.Question;
import io.bhishma.quiz.quizapp.model.UserQuizResult;
import io.bhishma.quiz.quizapp.repository.ChoiceRepository;
import io.bhishma.quiz.quizapp.repository.QuestionRepository;
import io.bhishma.quiz.quizapp.repository.UserQuizStatusRepository;

@Service
public class QuestionServiceImpl implements QuestionService {
	
	@Autowired
	private QuestionRepository questionRepository;
	
	@Autowired
	private ChoiceRepository choiceRepository;
	
	@Autowired
	private UserQuizStatusRepository userQuizRepository;
	
	@Override
	public List<QuestionEntity> findByQuizId(final Integer quizId) {	
		final List<QuestionEntity> questions = questionRepository.findByQuizId(quizId);
		return questions;
	}
	
	@Override
	public Question findByQuizIdAndOrder(final Integer quizId, final Integer order) {	
		final List<QuestionEntity> questions = questionRepository.findByQuizIdAndOrder(quizId, order);
		
		if(!CollectionUtils.isEmpty(questions) && questions.size() == 1) {
			
			final QuestionEntity questionEntity = questions.get(0);
			final List<ChoiceEntity> choices = choiceRepository.findByQuestionIdOrderByOrderAsc(questionEntity.getId());
			final List<UserQuizStatusEntity> userQuizList = userQuizRepository.findByUserIdAndQuizId(1, quizId);
			
			final Question question =  buildQuestion(questionEntity, choices);
			
			if(!CollectionUtils.isEmpty(userQuizList)) {
				int attemptedQuestions = userQuizList.size();
				buildUserQuizStatusResult(question, attemptedQuestions);				
			}	
			return question;
		}	
		
		throw new RuntimeException("Problem accessing questions. Please try again");
	}
	
	private Question buildQuestion(final QuestionEntity questionEntity , final List<ChoiceEntity> choiceEntities) {
		

		final Question question = new Question();
		final List<Choice> choices = new ArrayList<Choice>();
		
		question.setId(questionEntity.getId());
		question.setOrder(questionEntity.getOrder());
		question.setText(questionEntity.getText());
		question.setQuizId(questionEntity.getQuizId());
		
		for(final ChoiceEntity choiceEntity : choiceEntities) {
			
			final Choice choice = new Choice();
			choice.setId(choiceEntity.getId());
			choice.setOrder(choiceEntity.getOrder());
			choice.setText(choiceEntity.getText());
			choices.add(choice);
		}
		question.setChoices(choices);
		return question;
		
	}
	
	private Question buildUserQuizStatusResult(final Question question, final Integer attemptedQuestions) {
		
		final UserQuizResult userQuizResult = new UserQuizResult();
		userQuizResult.setAttempedQuestions(attemptedQuestions);
		userQuizResult.setNotAttempted(10-attemptedQuestions);
		userQuizResult.setTotalQuestions(10);
		question.setUserQuizResult(userQuizResult);
		
		return question;	
		
	}

}
