package io.bhishma.quiz.quizapp.service;

import io.bhishma.quiz.quizapp.entity.QuizEntity;
import io.bhishma.quiz.quizapp.model.QuizResult;
import io.bhishma.quiz.quizapp.model.UserQuiz;

public interface UserQuizService {
	
	void update(UserQuiz userQuiz);	
	QuizResult getResult(int userId, int quizId);
	QuizEntity loadQuiz(int userId);
	
}
