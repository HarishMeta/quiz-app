package io.bhishma.quiz.quizapp.service;

import io.bhishma.quiz.quizapp.entity.UserEntity;
import io.bhishma.quiz.quizapp.model.Quiz;

public interface QuizService {
	
	public void create(Quiz quiz);
	public UserEntity findUserByEmail(String email);
	
}
