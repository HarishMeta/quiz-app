package io.bhishma.quiz.quizapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import io.bhishma.quiz.quizapp.entity.UserQuizStatusEntity;
import io.bhishma.quiz.quizapp.model.UserAnswer;
import io.bhishma.quiz.quizapp.repository.UserQuizStatusRepository;

@Component
public class AnswerServiceImpl implements AnswerService {
	
	@Autowired
	private UserQuizStatusRepository userQuizStatusRepository;

	@Override
	@Transactional
	public void save(UserAnswer userAnswer) {
		
		List<UserQuizStatusEntity> list = userQuizStatusRepository.findByUserIdAndQuizIdAndQuestionId(userAnswer.getUserId(), userAnswer.getQuizId(), 
				userAnswer.getQuestionId());
		
		if(!CollectionUtils.isEmpty(list)) {
			userQuizStatusRepository.updateUserQuizStatus(userAnswer.getChoiceId(), userAnswer.getUserId(), 
						userAnswer.getQuizId(), userAnswer.getQuestionId());	
		}else {			
			final UserQuizStatusEntity userQuizStatusEntity = new UserQuizStatusEntity();
			userQuizStatusEntity.setChoiceId(userAnswer.getChoiceId());
			userQuizStatusEntity.setQuestionId(userAnswer.getQuestionId());
			userQuizStatusEntity.setQuizId(userAnswer.getQuizId());
			userQuizStatusEntity.setUserId(userAnswer.getUserId());
			userQuizStatusRepository.save(userQuizStatusEntity);	
		}
	}

}
