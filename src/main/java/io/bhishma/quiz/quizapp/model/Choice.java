package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;

public class Choice implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3882192282136876244L;
	private Integer id;
	private String text;
	private Integer order;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}


}
