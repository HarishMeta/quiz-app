package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;

public class UserQuizResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -321167893732234408L;
	
	private Integer userId;
	private Integer quizId;
	private Integer attempedQuestions;
	private Integer notAttempted;
	private Integer totalQuestions;
	private Integer correctAnswers;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getQuizId() {
		return quizId;
	}
	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}
	public Integer getAttempedQuestions() {
		return attempedQuestions;
	}
	public void setAttempedQuestions(Integer attempedQuestions) {
		this.attempedQuestions = attempedQuestions;
	}
	public Integer getNotAttempted() {
		return notAttempted;
	}
	public void setNotAttempted(Integer notAttempted) {
		this.notAttempted = notAttempted;
	}
	public Integer getTotalQuestions() {
		return totalQuestions;
	}
	public void setTotalQuestions(Integer totalQuestions) {
		this.totalQuestions = totalQuestions;
	}
	public Integer getCorrectAnswers() {
		return correctAnswers;
	}
	public void setCorrectAnswers(Integer correctAnswers) {
		this.correctAnswers = correctAnswers;
	}
	
	

}
