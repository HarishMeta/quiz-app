package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;

public class UserAnswer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2122809271919366458L;
	
	private Integer quizId;
	private Integer choiceId;
	private Integer questionId;
	private Integer userId;
	
	public Integer getQuizId() {
		return quizId;
	}
	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}
	public Integer getChoiceId() {
		return choiceId;
	}
	public void setChoiceId(Integer choiceId) {
		this.choiceId = choiceId;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}


}
