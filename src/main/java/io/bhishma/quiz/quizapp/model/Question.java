package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;
import java.util.List;

public class Question implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8106255513485366987L;
	private Integer id;
	private String text;
	private Integer order;
	private Integer quizId;
	private List<Choice> choices;
	private UserQuizResult userQuizResult;

	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}	
	
	public Integer getQuizId() {
		return quizId;
	}
	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}
	public List<Choice> getChoices() {
		return choices;
	}
	public void setChoices(List<Choice> choices) {
		this.choices = choices;
	}
	public UserQuizResult getUserQuizResult() {
		return userQuizResult;
	}
	public void setUserQuizResult(UserQuizResult userQuizResult) {
		this.userQuizResult = userQuizResult;
	}
	

}
