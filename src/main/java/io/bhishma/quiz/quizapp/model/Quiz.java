package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;

public class Quiz implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8031868989798129653L;
	
	private String name;
	private String description;
	private Integer id;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
