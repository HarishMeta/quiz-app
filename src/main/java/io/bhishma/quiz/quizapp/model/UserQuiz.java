package io.bhishma.quiz.quizapp.model;

import java.io.Serializable;

public class UserQuiz implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7723609279251077942L;
	
	private Integer userId;
	private Integer quizId;
	private String status;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getQuizId() {
		return quizId;
	}
	public void setQuizId(Integer quizId) {
		this.quizId = quizId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
