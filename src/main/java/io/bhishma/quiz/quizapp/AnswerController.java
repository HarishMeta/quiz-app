package io.bhishma.quiz.quizapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bhishma.platform.boot.user.UserContext;
import io.bhishma.quiz.quizapp.entity.UserEntity;
import io.bhishma.quiz.quizapp.model.UserAnswer;
import io.bhishma.quiz.quizapp.service.AnswerService;
import io.bhishma.quiz.quizapp.service.QuizService;

@RestController
@RequestMapping("/api/v1/quiz/questions/answer")
public class AnswerController {
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private QuizService quizService;
	
	
	@PostMapping
	public ResponseEntity<Object> record(@RequestBody final UserAnswer userAnswer) {
		UserEntity ue = quizService.findUserByEmail(UserContext.getUser().getEmail());
		userAnswer.setUserId( ue.getId());
		answerService.save(userAnswer);
		return new ResponseEntity<Object>(userAnswer, HttpStatus.OK);		
	}


}
