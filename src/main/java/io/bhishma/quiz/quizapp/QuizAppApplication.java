package io.bhishma.quiz.quizapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.bhishma.platform.boot.config.CorePlatformInitializer;

@SpringBootApplication
public class QuizAppApplication extends CorePlatformInitializer {

	public static void main(String[] args) {
		SpringApplication.run(QuizAppApplication.class, args);
	}

}
