package io.bhishma.quiz.quizapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.bhishma.quiz.quizapp.entity.QuestionEntity;

public interface QuestionRepository extends CrudRepository<QuestionEntity, Long> {
	
	List<QuestionEntity> findByQuizId(Integer quizId);
	
	List<QuestionEntity> findByQuizIdAndOrder(Integer quizId, Integer order);

}
