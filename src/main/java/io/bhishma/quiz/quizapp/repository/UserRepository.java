package io.bhishma.quiz.quizapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.bhishma.quiz.quizapp.entity.UserEntity;


public interface UserRepository extends CrudRepository<io.bhishma.quiz.quizapp.entity.UserEntity, Long>{
	
	List<UserEntity> findByEmail(String email);

}

