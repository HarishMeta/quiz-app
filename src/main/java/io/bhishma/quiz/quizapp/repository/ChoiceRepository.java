package io.bhishma.quiz.quizapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.bhishma.quiz.quizapp.entity.ChoiceEntity;

public interface ChoiceRepository extends CrudRepository<ChoiceEntity, Long> { 
	
	List<ChoiceEntity> findByQuestionIdOrderByOrderAsc(Integer questionId);
	
}
