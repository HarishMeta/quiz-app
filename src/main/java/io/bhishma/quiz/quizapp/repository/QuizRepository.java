package io.bhishma.quiz.quizapp.repository;

import org.springframework.data.repository.CrudRepository;

import io.bhishma.quiz.quizapp.entity.QuizEntity;

public interface QuizRepository extends CrudRepository<QuizEntity, Long>{

}
