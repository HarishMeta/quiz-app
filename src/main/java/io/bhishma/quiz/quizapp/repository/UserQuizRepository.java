package io.bhishma.quiz.quizapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import io.bhishma.quiz.quizapp.entity.UserQuizEntity;

public interface UserQuizRepository extends CrudRepository<UserQuizEntity, Long>{
	
	
	List<UserQuizEntity> findByUserIdAndQuizId(Integer userId, Integer quizId);
	
	List<UserQuizEntity> findByUserId(Integer userId);

	@Modifying
	@Query(value = "UPDATE quizapp.user_quiz uq set status =:status where uq.user_id = :userId and uq.quiz_id = :quizId",
	            nativeQuery = true)
	void updateUser(@Param("status") String status, @Param("userId") Integer userId, @Param("quizId") Integer quizId);

}
