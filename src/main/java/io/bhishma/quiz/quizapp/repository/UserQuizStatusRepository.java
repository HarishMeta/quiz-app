package io.bhishma.quiz.quizapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import io.bhishma.quiz.quizapp.entity.UserQuizStatusEntity;

public interface UserQuizStatusRepository extends CrudRepository<UserQuizStatusEntity, Long> {
	
	List<UserQuizStatusEntity> findByUserIdAndQuizId(Integer userId, Integer quizId);
	List<UserQuizStatusEntity> findByUserIdAndQuizIdAndQuestionId(Integer userId, Integer quizId, Integer questionId);
	
	@Modifying
	@Query(value = "UPDATE quizapp.user_quiz_status uqs set choice_id =:choiceId where uqs.user_id = :userId and uqs.quiz_id = :quizId and uqs.question_id = :questionId",
	            nativeQuery = true)
	void updateUserQuizStatus(@Param("choiceId") Integer choiceId, @Param("userId") Integer userId, 
					@Param("quizId") Integer quizId, @Param("questionId") Integer questionId );


}
