package io.bhishma.quiz.quizapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bhishma.platform.boot.user.User;
import io.bhishma.platform.boot.user.UserContext;
import io.bhishma.quiz.quizapp.entity.QuizEntity;
import io.bhishma.quiz.quizapp.entity.UserEntity;
import io.bhishma.quiz.quizapp.model.Quiz;
import io.bhishma.quiz.quizapp.model.QuizResult;
import io.bhishma.quiz.quizapp.model.UserQuiz;
import io.bhishma.quiz.quizapp.service.QuizService;
import io.bhishma.quiz.quizapp.service.UserQuizService;

@RestController
@RequestMapping("/api/v1/quiz")
public class QuizController {
	
	@Autowired
	private QuizService quizService;
	
	@Autowired
	private UserQuizService userQuizService;
	
	@PostMapping
	public ResponseEntity<Object> create(@RequestBody final Quiz quiz) {
		quizService.create(quiz);
		return new ResponseEntity<Object>(quiz, HttpStatus.OK);		
	}
	
	@GetMapping
	public ResponseEntity<Object> loadQuiz() {
		User user = UserContext.getUser();		
		UserEntity user1 = quizService.findUserByEmail(user.getEmail());
		QuizEntity quiz = userQuizService.loadQuiz(user1.getId());
		return new ResponseEntity<Object>(quiz, HttpStatus.OK);		
	}
	
	@PostMapping("/submit")
	public ResponseEntity<QuizResult> submit(@RequestBody final Quiz quiz) {
		User user = UserContext.getUser();
		UserEntity userResponse = quizService.findUserByEmail(user.getEmail());
		UserQuiz uq = new UserQuiz();
		uq.setQuizId(quiz.getId());
		uq.setStatus("COMPLETED");
		uq.setUserId(userResponse.getId());
		userQuizService.update(uq);
		return new ResponseEntity<QuizResult>(userQuizService.getResult(userResponse.getId(), quiz.getId()), 
				HttpStatus.OK);		
	}


}
